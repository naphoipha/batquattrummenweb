import "./App.css";
import WelcomePage from "./Page/WelcomePage/WelcomePage";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import BlogHomePage from "./Page/BlogHomePage/BlogHomePage";
import ExperiencePage from "./Page/ExperiencePage/ExperiencePage";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<WelcomePage />} />
          <Route path="/bloghomepage" element={<BlogHomePage />} />
          <Route path="/experiencehomepage" element={<ExperiencePage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
