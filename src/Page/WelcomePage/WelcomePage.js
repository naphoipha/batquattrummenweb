import React from "react";
import "./WelcomePage.css";
import { NavLink } from "react-router-dom";

export default function WelcomePage() {
  return (
    <div className="welcome-background bg-gradient-to-r from-indigo-500 via-purple-500 to-pink-500 py-60">
      <div className="welcome-title animate__animated animate__fadeInDown">
        BẬT QUẠT TRÙM MỀN'S BLOG
      </div>
      <div className="flex justify-center items-center gap-4 pt-4 animate__animated animate__fadeInDown animate__delay-1s	">
        <NavLink to="/experiencehomepage">
          <button className="bg-transparent hover:bg-blue-500 text-white font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full">
            Trải Nghiệm
          </button>
        </NavLink>
        <NavLink to="/bloghomepage">
          <button className="bg-transparent hover:bg-blue-500 text-white font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-full">
            Thông Tin
          </button>
        </NavLink>
      </div>
    </div>
  );
}
